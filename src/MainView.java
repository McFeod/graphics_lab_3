import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLJPanel;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

public class MainView extends GLJPanel implements GLEventListener, KeyListener {
    private GLCube cube;
    @Override
    public void init(GLAutoDrawable drawable) {
        cube = new GLCube(drawable);
    }
    @Override
    public void dispose(GLAutoDrawable drawable) {
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        cube.render();
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        cube.setSize(width, height);
    }
    public static void main(String[] args){
        final GLProfile profile;
        profile = GLProfile.get(GLProfile.GL2);
        GLCapabilities capabilities = new GLCapabilities (profile);
        MainView canvas = new MainView(capabilities);
        final JFrame frame = new JFrame ("textures");
        frame.getContentPane().add(canvas);
        frame.setSize(frame.getContentPane().getPreferredSize());
        frame.setVisible(true);
        JPanel p = new JPanel();
        p.setPreferredSize(new Dimension(0,0));
        frame.add(p,BorderLayout.SOUTH);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        int key = keyEvent.getKeyCode();
        switch (key){
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_A: cube.rotate('y', -5); break;
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_D: cube.rotate('y', 5); break;
            case KeyEvent.VK_UP:
            case KeyEvent.VK_W: cube.rotate('x', -5); break;
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_S: cube.rotate('x', 5); break;
            case KeyEvent.VK_PAGE_UP :
            case KeyEvent.VK_Q: cube.rotate('z', -5); break;
            case KeyEvent.VK_PAGE_DOWN:
            case KeyEvent.VK_E: cube.rotate('z', 5); break;
        }
        repaint();
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }

    MainView(GLCapabilities capabilities){
        super(capabilities);
        setPreferredSize(new Dimension(600,600));
        addGLEventListener(this);
        addKeyListener(this);
    }
}