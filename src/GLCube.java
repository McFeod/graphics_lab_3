import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.glu.GLU;
import java.io.File;
import java.io.IOException;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toRadians;


public class GLCube {
    private char last_axis = ' ';
    private GL2 gl2;
    private GLU glu;
    private float rotationX, rotationY, rotationZ;
    private int texture;

    public GLCube(GLAutoDrawable drawable){
        glu = new GLU();
        gl2 = drawable.getGL().getGL2();
        gl2.glClearColor(0f,0f,0f,1.0f);
        gl2.glClearDepth(1.0f);
        gl2.glEnable(GL2.GL_CULL_FACE);
        gl2.glEnable(GL2.GL_DEPTH_TEST);

        try{
            File im = new File("images/dice_texture.png");
            Texture t = TextureIO.newTexture(im,true);
            texture = t.getTextureObject(gl2);
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    public void setSize(int width, int height){
        float h = (height > 1) ? (float)width/(float)height : 1;
        gl2.glViewport(0, 0, width, height);
        gl2.glMatrixMode(GL2.GL_PROJECTION);
        gl2.glLoadIdentity();
        glu.gluPerspective(45.0f, h, 1.0, 20.0);
        gl2.glMatrixMode(GL2.GL_MODELVIEW);
        gl2.glLoadIdentity();
    }

    public void rotate(char type, float val) {
        last_axis = type;
        switch (last_axis){
            case 'x': rotationX += val; break;
            case 'y': rotationY += val; break;
            case 'z': rotationZ += val; break;
        }
    }

    public void render(){
        gl2.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
        gl2.glMatrixMode(GL2.GL_MODELVIEW);
        gl2.glLoadIdentity();
        gl2.glTranslatef(0f,0f,-5.0f);

        gl2.glRotatef(rotationX, 1.0f, 0.0f, 0.0f);
        gl2.glRotatef(rotationY, 0.0f, 1.0f, 0.0f);
        gl2.glRotatef(rotationZ, 0.0f, 0.0f, 1.0f);
        gl2.glEnable(GL2.GL_TEXTURE_2D);
        gl2.glBindTexture(GL2.GL_TEXTURE_2D, texture);
        gl2.glBegin(GL2.GL_QUADS);
        // 5, спереди
        gl2.glTexCoord2f(0.0f, 0.25f);  gl2.glVertex3f(-1.0f, -1.0f, 1.0f);
        gl2.glTexCoord2f(0.25f, 0.25f); gl2.glVertex3f(1.0f, -1.0f, 1.0f);
        gl2.glTexCoord2f(0.25f, 0.5f);  gl2.glVertex3f(1.0f, 1.0f, 1.0f);
        gl2.glTexCoord2f(0.0f, 0.5f);   gl2.glVertex3f(-1.0f, 1.0f, 1.0f);
        // 2, сзади
        gl2.glTexCoord2f(0.75f, 0.25f); gl2.glVertex3f(-1.0f, -1.0f, -1.0f);
        gl2.glTexCoord2f(0.75f, 0.5f);  gl2.glVertex3f(-1.0f, 1.0f, -1.0f);
        gl2.glTexCoord2f(0.5f, 0.5f);   gl2.glVertex3f(1.0f, 1.0f, -1.0f);
        gl2.glTexCoord2f(0.5f, 0.25f);  gl2.glVertex3f(1.0f, -1.0f, -1.0f);
        // 6, сверху
        gl2.glTexCoord2f(0.25f, 0.75f); gl2.glVertex3f(-1.0f, 1.0f, -1.0f);
        gl2.glTexCoord2f(0.25f, 0.5f);  gl2.glVertex3f(-1.0f, 1.0f, 1.0f);
        gl2.glTexCoord2f(0.5f, 0.5f);   gl2.glVertex3f(1.0f, 1.0f, 1.0f);
        gl2.glTexCoord2f(0.5f, 0.75f);  gl2.glVertex3f(1.0f, 1.0f, -1.0f);
        // 1, снизу
        gl2.glTexCoord2f(0.5f, 0.25f);  gl2.glVertex3f(-1.0f, -1.0f, -1.0f);
        gl2.glTexCoord2f(0.25f, 0.25f); gl2.glVertex3f(1.0f, -1.0f, -1.0f);
        gl2.glTexCoord2f(0.25f, 0.0f);  gl2.glVertex3f(1.0f, -1.0f, 1.0f);
        gl2.glTexCoord2f(0.5f, 0.0f);   gl2.glVertex3f(-1.0f, -1.0f, 1.0f);
        // 3, справа
        gl2.glTexCoord2f(0.5f, 0.25f);  gl2.glVertex3f(1.0f, -1.0f, -1.0f);
        gl2.glTexCoord2f(0.5f, 0.5f);   gl2.glVertex3f(1.0f, 1.0f, -1.0f);
        gl2.glTexCoord2f(0.25f, 0.5f);  gl2.glVertex3f(1.0f, 1.0f, 1.0f);
        gl2.glTexCoord2f(0.25f, 0.25f); gl2.glVertex3f(1.0f, -1.0f, 1.0f);
        // 4, слева
        gl2.glTexCoord2f(0.75f, 0.25f); gl2.glVertex3f(-1.0f, -1.0f, -1.0f);
        gl2.glTexCoord2f(1.0f, 0.25f);  gl2.glVertex3f(-1.0f, -1.0f, 1.0f);
        gl2.glTexCoord2f(1.0f, 0.5f);   gl2.glVertex3f(-1.0f, 1.0f, 1.0f);
        gl2.glTexCoord2f(0.75f, 0.5f);  gl2.glVertex3f(-1.0f, 1.0f, -1.0f);

        gl2.glEnd();

        switch (last_axis){
            // для осей x и y нужна система координат наблюдателя
            case 'x': case 'y':
                gl2.glPushMatrix();
                gl2.glLoadIdentity();
                gl2.glTranslatef(0f, 0f, -5.0f);
                drawAxis();
                gl2.glPopMatrix();
                break;
            // для z - самого кубика
            case 'z':
                drawAxis();
        }

    }

    // непосредственная прорисовка осей
    private void drawAxis(){
        gl2.glDisable( GL2.GL_TEXTURE_2D );
        gl2.glBegin(GL2.GL_LINES);
        gl2.glColor3f(1.0f, 1.0f, 1.0f);
        switch (last_axis){
            case 'x':
                gl2.glVertex3f(-5, 0, 0);
                gl2.glVertex3f(5, 0, 0);
                break;
            case 'y':
                System.out.println();
                gl2.glVertex3f(0, (float)(-5*cos(toRadians(rotationX%360) )),
                                  (float)(-5*sin(toRadians(rotationX%360) )));
                gl2.glVertex3f(0, (float)(5*cos(toRadians(rotationX%360))),
                                  (float)(5*sin(toRadians(rotationX%360))));
                break;
            case 'z':
                gl2.glVertex3f(0, 0, -5);
                gl2.glVertex3f(0, 0, 5);
                break;
        }
        gl2.glEnd();
    }
}
